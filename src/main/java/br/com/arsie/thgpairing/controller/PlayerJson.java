package br.com.arsie.thgpairing.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PlayerJson {

    @JsonProperty("name")
    private String name;

}
