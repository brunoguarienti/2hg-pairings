package br.com.arsie.thgpairing.controller;

import br.com.arsie.thgpairing.domain.Pairing;
import br.com.arsie.thgpairing.domain.Player;
import br.com.arsie.thgpairing.domain.Round;
import br.com.arsie.thgpairing.persistence.PlayerPersistence;
import br.com.arsie.thgpairing.persistence.RoundPersistence;
import br.com.arsie.thgpairing.service.RoundCreationService;
import br.com.arsie.thgpairing.service.SetResultService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tournament")
@RequiredArgsConstructor
public class TournamentController {

    private final PlayerPersistence playerPersistence;
    private final RoundPersistence roundPersistence;
    private final RoundCreationService roundCreationService;
    private final SetResultService setResultService;

    @GetMapping(value = "/player")
    public ResponseEntity getPlayers() {
        List<Player> players = playerPersistence.findAll();

        return new ResponseEntity(players, HttpStatus.OK);
    }

    @PostMapping(value = "/player")
    public ResponseEntity addPlayer(@RequestBody PlayerJson playerJson) {
        Player newPlayer = new Player();
        newPlayer.setName(playerJson.getName());

        return new ResponseEntity(playerPersistence.save(newPlayer), HttpStatus.CREATED);
    }

    @GetMapping(value = "/round")
    public ResponseEntity getRounds() {
        List<Round> rounds = roundPersistence.findAll();

        return new ResponseEntity(rounds, HttpStatus.OK);
    }

    @GetMapping(value = "/round/{id}")
    public ResponseEntity getRound(@PathVariable Long id) {
        Optional<Round> roundOptional = roundPersistence.findById(id);

        if (roundOptional.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(roundOptional.get(), HttpStatus.OK);
    }

    @GetMapping(value = "/round/{id}/missing-results")
    public ResponseEntity getMissingRoundResults(@PathVariable Long id) {
        Optional<Round> roundOptional = roundPersistence.findById(id);

        if (roundOptional.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        Round round = roundOptional.get();
        List<Pairing> missingResults = new ArrayList<>();
        for (Pairing pairing : round.getPairingList()){
            if(pairing.getResults() == null){
                missingResults.add(pairing);
            }
        }
        round.setPairingList(missingResults);

        return new ResponseEntity(roundOptional.get(), HttpStatus.OK);
    }

    @PostMapping(value = "/round")
    public ResponseEntity createRound() {
        Round round = roundCreationService.createRound();

        return new ResponseEntity(round, HttpStatus.CREATED);
    }

    @PostMapping(value = "/pairing/{id}")
    public ResponseEntity setResult(@PathVariable Long id, @RequestBody ResultJson resultJson) {
        Optional<Pairing> pairing = setResultService.setResult(id, resultJson);

        if (pairing.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(pairing.get(), HttpStatus.CREATED);
    }

}
