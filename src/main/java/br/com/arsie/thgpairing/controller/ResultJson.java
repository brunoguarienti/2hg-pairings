package br.com.arsie.thgpairing.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ResultJson {

    @JsonProperty("a_team")
    Integer playersAWins;
    @JsonProperty("b_team")
    Integer playersBWins;

}
