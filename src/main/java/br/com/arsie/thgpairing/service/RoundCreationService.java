package br.com.arsie.thgpairing.service;

import br.com.arsie.thgpairing.domain.Pairing;
import br.com.arsie.thgpairing.domain.Player;
import br.com.arsie.thgpairing.domain.PlayerTuple;
import br.com.arsie.thgpairing.domain.Round;
import br.com.arsie.thgpairing.persistence.PlayerPersistence;
import br.com.arsie.thgpairing.persistence.RoundPersistence;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class RoundCreationService {

    private final PlayerPersistence playerPersistence;
    private final RoundPersistence roundPersistence;

    private Integer breakQty;

    public Round createRound() {
        Long totalPlayers = playerPersistence.count();
        breakQty = 0;
        if (totalPlayers < 4) {
            throw new NotEnoughtPlayersException("Not enough players: " + totalPlayers + " (at least 4 needed)");
        }

        List<Round> previousRounds = roundPersistence.findAll();

        for (Round round : previousRounds) {
            for (Pairing pairing : round.getPairingList()) {
                if (pairing.getResults() == null) {
                    throw new MissingResultException("Missing results on round #" + round.getOrdinal() + " (id: " + round.getId() + ")");
                }
            }
        }

        List<PlayerTuple> tuples = generateTuples(playerPersistence.findAll(), previousRounds);
        Round round = new Round();
        round.setOrdinal(previousRounds.size() + 1);
        round.setPairingList(generatePairings(tuples));
        return roundPersistence.save(round);
    }

    private List<Pairing> generatePairings(List<PlayerTuple> tuples) {
        List<Pairing> pairings = new ArrayList<>();

        Random random = new Random();

        while (tuples.size() > 1) {
            PlayerTuple playerTupleA = tuples.remove(random.nextInt(tuples.size()));
            PlayerTuple playerTupleB = tuples.remove(random.nextInt(tuples.size()));

            pairings.add(new Pairing(playerTupleA, playerTupleB));
        }

        if (tuples.size() == 1) {
            pairings.add(new Pairing(tuples.get(0)));
        }

        return pairings;
    }

    private List<PlayerTuple> generateTuples(List<Player> originalPlayers, List<Round> previousRounds) {
        List<PlayerTuple> playerTuples;
        List<Player> players = new ArrayList<>(originalPlayers);

        Random random = new Random();

        if (previousRounds.isEmpty()) {
            playerTuples = new ArrayList<>();
            while (players.size() > 1) {
                Player playerA = players.remove(random.nextInt(players.size()));
                Player playerB = players.remove(random.nextInt(players.size()));

                playerTuples.add(new PlayerTuple(playerA, playerB));
            }

            if (players.size() == 1) {
                playerTuples.add(new PlayerTuple(players.get(0)));
            }
        } else {
            List<PlayerTuple> previousTuples = new ArrayList<>();

            for (Round round : previousRounds) {
                for (Pairing pairing : round.getPairingList()) {
                    previousTuples.add(pairing.getPlayersA());
                    previousTuples.add(pairing.getPlayersB());
                }
            }

            do {
                playerTuples = new ArrayList<>();
                players = new ArrayList<>(originalPlayers);

                while (players.size() > 1) {
                    Player playerA = players.remove(random.nextInt(players.size()));
                    Player playerB = players.remove(random.nextInt(players.size()));

                    playerTuples.add(new PlayerTuple(playerA, playerB));
                }

                if (players.size() == 1) {
                    playerTuples.add(new PlayerTuple(players.get(0)));
                }

            } while (checkDuplicates(previousTuples, playerTuples));

        }

        return playerTuples;
    }

    private boolean checkDuplicates(List<PlayerTuple> previousTuples, List<PlayerTuple> playerTuples) {
        if (breakQty++ == 100) {
            return false;
        }

        return previousTuples.stream()
                .map(playerTuple -> checkDuplicates(playerTuple, playerTuples))
                .collect(Collectors.toList())
                .stream().parallel().anyMatch(aBoolean -> aBoolean);
    }

    private boolean checkDuplicates(PlayerTuple playerTuple, List<PlayerTuple> playerTuples) {
        return playerTuples.stream().parallel().anyMatch(pt -> pt.equals(playerTuple));
    }
}
