package br.com.arsie.thgpairing.service;

import br.com.arsie.thgpairing.controller.ResultJson;
import br.com.arsie.thgpairing.domain.MatchResults;
import br.com.arsie.thgpairing.domain.Pairing;
import br.com.arsie.thgpairing.persistence.PairingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class SetResultService {

    private final PairingRepository pairingRepository;

    public Optional<Pairing> setResult(Long id, ResultJson resultJson) {
        Optional<Pairing> pairingOptional = pairingRepository.findById(id);

        if (pairingOptional.isEmpty()) {
            return pairingOptional;
        }

        Pairing pairing = pairingOptional.get();
        pairing.setResults(new MatchResults(resultJson.getPlayersAWins(), resultJson.getPlayersBWins()));

        setPoints(pairing, resultJson);

        return Optional.of(pairingRepository.save(pairing));
    }

    private void setPoints(Pairing pairing, ResultJson result) {
        if (result.getPlayersAWins() > result.getPlayersBWins()) {
            if (pairing.getPlayersA() != null && pairing.getPlayersA().getPlayerA() != null) {
                pairing.getPlayersA().getPlayerA().setPoints(pairing.getPlayersA().getPlayerA().getPoints() + 3);
            }
            if (pairing.getPlayersA() != null && pairing.getPlayersA().getPlayerB() != null) {
                pairing.getPlayersA().getPlayerB().setPoints(pairing.getPlayersA().getPlayerB().getPoints() + 3);
            }
        } else {
            if (result.getPlayersAWins() < result.getPlayersBWins()) {
                if (pairing.getPlayersB() != null && pairing.getPlayersA().getPlayerA() != null) {
                    pairing.getPlayersB().getPlayerA().setPoints(pairing.getPlayersB().getPlayerA().getPoints() + 3);
                }
                if (pairing.getPlayersB() != null && pairing.getPlayersA().getPlayerB() != null) {
                    pairing.getPlayersB().getPlayerB().setPoints(pairing.getPlayersB().getPlayerB().getPoints() + 3);
                }
            } else {
                if (pairing.getPlayersA() != null && pairing.getPlayersA().getPlayerA() != null) {
                    pairing.getPlayersA().getPlayerA().setPoints(pairing.getPlayersA().getPlayerA().getPoints() + 1);
                }
                if (pairing.getPlayersA() != null && pairing.getPlayersA().getPlayerB() != null) {
                    pairing.getPlayersA().getPlayerB().setPoints(pairing.getPlayersA().getPlayerB().getPoints() + 1);
                }
                if (pairing.getPlayersB() != null && pairing.getPlayersA().getPlayerA() != null) {
                    pairing.getPlayersB().getPlayerA().setPoints(pairing.getPlayersB().getPlayerA().getPoints() + 1);
                }
                if (pairing.getPlayersB() != null && pairing.getPlayersA().getPlayerB() != null) {
                    pairing.getPlayersB().getPlayerB().setPoints(pairing.getPlayersB().getPlayerB().getPoints() + 1);
                }
            }
        }
    }
}
