package br.com.arsie.thgpairing.service;

public class MissingResultException extends RuntimeException {
    public MissingResultException(String message) {
        super(message);
    }
}
