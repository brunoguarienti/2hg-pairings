package br.com.arsie.thgpairing.service;

public class NotEnoughtPlayersException extends RuntimeException {
    public NotEnoughtPlayersException(String message) {
        super(message);
    }
}
