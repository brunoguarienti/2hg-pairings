package br.com.arsie.thgpairing.persistence;

import br.com.arsie.thgpairing.domain.Pairing;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PairingRepository extends JpaRepository<Pairing, Long> {
}
