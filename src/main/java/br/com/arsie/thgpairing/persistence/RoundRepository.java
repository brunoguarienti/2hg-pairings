package br.com.arsie.thgpairing.persistence;

import br.com.arsie.thgpairing.domain.Round;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoundRepository extends JpaRepository<Round, Long> {
}
