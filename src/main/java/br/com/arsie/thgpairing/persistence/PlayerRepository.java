package br.com.arsie.thgpairing.persistence;

import br.com.arsie.thgpairing.domain.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Long> {
}
