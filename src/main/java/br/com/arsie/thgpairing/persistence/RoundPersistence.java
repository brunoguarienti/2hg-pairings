package br.com.arsie.thgpairing.persistence;

import br.com.arsie.thgpairing.domain.Round;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class RoundPersistence {

    private final RoundRepository roundRepository;

    public List<Round> findAll() {
        return roundRepository.findAll();
    }

    public Round save(Round round) {
        return roundRepository.save(round);
    }

    public Optional<Round> findById(Long id) {
        return roundRepository.findById(id);
    }
}
