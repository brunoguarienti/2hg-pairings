package br.com.arsie.thgpairing.persistence;

import br.com.arsie.thgpairing.domain.Player;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
@RequiredArgsConstructor
@Transactional
public class PlayerPersistence {

    private final PlayerRepository playerRepository;

    public List<Player> findAll() {
        return playerRepository.findAll(Sort.by(Sort.Direction.DESC, "points"));
    }

    public Player save(Player player) {
        return playerRepository.save(player);
    }

    public Long count() {
        return playerRepository.count();
    }
}
