package br.com.arsie.thgpairing.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
@Entity(name = "Round")
@Table(name = "thg_round")
public class Round {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    Integer ordinal;

    @OneToMany(cascade = CascadeType.ALL)
    List<Pairing> pairingList;

}
