package br.com.arsie.thgpairing.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@RequiredArgsConstructor
@Entity(name = "MatchResults")
@Table(name = "thg_match_results")
public class MatchResults {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    Integer playerTupleAWins;

    Integer playerTupleBWins;

    public MatchResults(Integer playersAWins, Integer playersBWins) {
        this.playerTupleAWins = playersAWins;
        this.playerTupleBWins = playersBWins;
    }
}
