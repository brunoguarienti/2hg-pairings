package br.com.arsie.thgpairing.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@RequiredArgsConstructor
@Entity(name = "Pairing")
@Table(name = "thg_pairing")
public class Pairing {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(
            name = "player_tuple_a_id"
    )
    PlayerTuple playersA;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(
            name = "player_tuple_b_id"
    )
    PlayerTuple playersB;

    @OneToOne(cascade = CascadeType.ALL)
    MatchResults results;

    public Pairing(PlayerTuple playerTupleA, PlayerTuple playerTupleB) {
        this.playersA = playerTupleA;
        this.playersB = playerTupleB;
    }

    public Pairing(PlayerTuple playerTupleA) {
        this.playersA = playerTupleA;
    }
}
