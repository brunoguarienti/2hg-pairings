package br.com.arsie.thgpairing.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@Entity(name = "PlayerTuple")
@Table(name = "thg_player_tuple")
@RequiredArgsConstructor
public class PlayerTuple {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(
            name = "player_a_id"
    )
    Player playerA;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(
            name = "player_b_id"
    )
    Player playerB;

    public PlayerTuple(Player playerA, Player playerB) {
        this.playerA = playerA;
        this.playerB = playerB;
    }

    public PlayerTuple(Player playerA) {
        this.playerA = playerA;
    }

    @Override
    public boolean equals(Object o) {
        if(o.getClass() != this.getClass()){
            return false;
        }

        PlayerTuple playerTuple = (PlayerTuple) o;
        if(this.getPlayerA().getId().equals(playerTuple.getPlayerA().getId())
                && this.getPlayerB().getId().equals(playerTuple.getPlayerB().getId())){
            return true;
        }

        return this.getPlayerA().getId().equals(playerTuple.getPlayerB().getId())
                && this.getPlayerB().getId().equals(playerTuple.getPlayerA().getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, playerA, playerB);
    }
}
